#!/usr/bin/env python3

# Standard libraries
from os import environ
from sys import version_info

# Components
from ..system.platform import Platform
from .bundle import Bundle

# Version class
class Version:

    # Getter
    @staticmethod
    def get() -> str:

        # Fake test version
        if Bundle.ENV_DEBUG_VERSION_FAKE in environ:
            return environ[Bundle.ENV_DEBUG_VERSION_FAKE]

        # Acquire version from metadata
        try:
            from importlib import metadata # pylint: disable=import-outside-toplevel
            return metadata.version(Bundle.PACKAGE)
        except Exception: # pylint: disable=broad-exception-caught # pragma: no cover
            pass

        # Acquire version from resources
        try: # pragma: no cover
            from pkg_resources import require # pylint: disable=import-outside-toplevel
            name = __name__.split('.', maxsplit=1)[0]
            return str(require(name)[0].version)

        # Default fallback
        except Exception: # pylint: disable=broad-exception-caught # pragma: no cover
            return '0.0.0'

    # Path
    @staticmethod
    def path() -> str:

        # Acquire path
        path = __file__

        # Strip package path
        index = path.rfind(Platform.PATH_SEPARATOR)
        index = path.rfind(Platform.PATH_SEPARATOR, 0, index)
        path = path[0:index]

        # Result
        return path

    # Python
    @staticmethod
    def python() -> str:

        # Acquire Python version
        version = f'{version_info.major}.{version_info.minor}'

        # Result
        return version
